// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "OneMoreTimesGameStateBase.generated.h"

// TODO: How do I get access to this in Blueprints?
UENUM(BlueprintType)
enum class ETimeManagementMode : uint8
{
	E_Record                     UMETA(DisplayName = "Record"),
	E_Rewind                     UMETA(DisplayName = "Rewind"),
	E_Replay                     UMETA(DisplayName = "Replay")
};

// add: current lifeforce, max lifeforce.
// get that working for single character play/rewind/replay/record.
// add interacting for more max mana.
// add saving game
// add clones

/**
 * 
 */
UCLASS()
class ONEMORETIMES_API AOneMoreTimesGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	
};
